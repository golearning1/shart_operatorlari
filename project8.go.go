 //Foydalanuvchi tomonidan son kiritiladi Decimal qiymatdam kelib chiqgan holda harflarning katta yoki kichikligini aniqlab 
 //beradigan dastur

package main 

import "fmt"

func main(){

	asciiValue := 0
	fmt.Println("Son kiriting: ")
	fmt.Scanln(&asciiValue)
	charakter := rune(asciiValue)

	fmt.Printf("Ascii Decimal %d => %c\n",asciiValue,charakter)
}