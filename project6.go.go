
// Uchta sonning yigindisini,kopaytmasini,o'rta arifmetigini,kattasi va kichigini aniqlab beradigan dastur 

package main 

import "fmt"

func main(){

	var s1,s2,s3,yigindi,kopaytma,ortalama,katta,kichik float32
	fmt.Println("Iltimos s1,s2,s3 larni ketma ketlikda kiriting: ")
	fmt.Scanf("%v%v%v",&s1,&s2,&s3)

	yigindi = s1 + s2 + s3
	kopaytma = s1 * s2 * s3
	ortalama = yigindi/3

	kichik = s1
	if s2 < kichik{
		kichik = s2
	}
	if s3 < kichik{
		kichik = s3
	}
	katta = s1
	if s2 > katta{
		katta = s2
	}
	if s3 > katta{
		katta = s3
	}

	fmt.Println("Yig'indi",yigindi,"Ko'paytma",kopaytma,"O'rta arifmetigi",ortalama)
	fmt.Println("Katta son",katta,"Kichik son",kichik)
}